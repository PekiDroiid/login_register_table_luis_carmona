export default interface IUsuario 
{
    id: number,
    email: string,
    password: string,
    name: string,
    age: number,
}
