import type IUsuario from '@/Interfaces/IUser'

const UsuariosData: IUsuario[] =
[
    {
        id: 1,
        email: 'usuario@hotmail.com',
        password: 'User1234@',
        name: 'User1',
        age: 21,
    },
    {
        id: 2,
        email: 'usuario2@hotmail.com',
        password: 'User1234@',
        name: 'User2',
        age: 22,
    }

]
export default UsuariosData
